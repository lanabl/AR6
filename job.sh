#!/bin/bash 
#SBATCH --qos=priority 
#SBATCH --job-name=job2
#SBATCH --account=tipes
#SBATCH --mem=10000
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --output=job-%j.log
#SBATCH --error=job-%j.err
#SBATCH --workdir=/home/mayayami/AR6/


srun python CMIP6_metric.py