from cdo import * 
cdo = Cdo()
import os
import glob
import xarray as xr
import numpy as np

def get_tos_paths_dict(exp='historical'):
    path_base = '/p/tmp/mayayami/SYNDA/data/CMIP6/CMIP/'
    institutes = os.listdir(path_base)
    paths_all_models = dict()

    for i in institutes:
        pi = path_base + i
        models = os.listdir(pi)
        for m in models:
            pim = pi + '/' + m + '/{}/'.format(exp)
            if glob.glob(pim):
                print(pim)
                emembers = os.listdir(pim)
                tmp = dict()
                for v in emembers:
                    paths = [x[0] for x in os.walk(pim+v)]
                    data_paths = []
                    for path in paths:
                        if glob.glob("{}/tos_Omon_*.nc".format(path)):
                            data_paths.append(glob.glob("{}/tos_Omon_*.nc".format(path)))
                    # if (len(data_paths)) == 0:
                    #     print("No paths provided for model '"+m+"' and ensemble member '"+v)
                    # elif (len(data_paths)) != 1:
                    #     print("Several paths provided for model '"+m+"' and ensemble member '"+v+"'!")
                    #     print(data_paths)
                    tmp[v] = data_paths
                paths_all_models[m] = tmp
    return paths_all_models

def get_pr_paths_dict(exp='historical'):
    path_base = '/p/tmp/mayayami/SYNDA/data/CMIP6/CMIP/'
    institutes = os.listdir(path_base)
    paths_all_models = dict()

    for i in institutes:
        pi = path_base + i
        models = os.listdir(pi)
        for m in models:
            pim = pi + '/' + m + '/{}/'.format(exp)
            if glob.glob(pim):
                print(pim)
                emembers = os.listdir(pim)
                tmp = dict()
                for v in emembers:
                    paths = [x[0] for x in os.walk(pim+v)]
                    data_paths = []
                    for path in paths:
                        if glob.glob("{}/pr_Amon_*.nc".format(path)):
                            data_paths.append(glob.glob("{}/pr_Amon_*.nc".format(path)))
                    # if (len(data_paths)) == 0:
                    #     print("No paths provided for model '"+m+"' and ensemble member '"+v)
                    # elif (len(data_paths)) != 1:
                    #     print("Several paths provided for model '"+m+"' and ensemble member '"+v+"'!")
                    #     print(data_paths)
                    tmp[v] = data_paths
                paths_all_models[m] = tmp
    return paths_all_models

def get_box_avg(data,lon_min,lon_max,lat_min,lat_max,str_lon, str_lat):
    mean_dims = [d for d in data.dims if d != "time"]
    print("   The mean is taken over the dimensions ", mean_dims)
    if np.sum(data[str_lon].values<0) > 0:
        lon_max_tmp = lon_max-360
        lon_min_tmp = lon_min-360
    else:
        lon_max_tmp = lon_max
        lon_min_tmp = lon_min
    data_box = (
        data
        .where(data[str_lon] >= lon_min_tmp)
        .where(data[str_lon] <= lon_max_tmp)
        .where(data[str_lat] >= lat_min)
        .where(data[str_lat] <= lat_max)
    )
    data_box[0].plot()
    box_avg = data_box.mean(dim = mean_dims)
    return box_avg