import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
from scipy.ndimage import gaussian_filter1d

def make_lambda_array(data,ws=70,gedge=5):
    lambdas = np.full((data.shape[0],data.shape[1],data.shape[2]-2*gedge),np.nan)
    for i in np.arange(0,data.shape[0]):
        for j in np.arange(0,data.shape[1]):
            amoc = np.nan_to_num(data[i,j].values)
            amoc_low = gaussian_filter1d(amoc, 10)
            if amoc.sum() != 0:
                lamb = run_fit_a_ar1((amoc-amoc_low)[gedge:-gedge],ws)
                lambdas[i,j,:]=lamb
    return lambdas

def make_pvs_array(lambdas,time,ws=70):
    bound = ws // 2
    tt = time[bound:-bound]
    lambda_trends = np.full((lambdas.shape[0],lambdas.shape[1]),np.nan)
    lambda_pvs = np.full((lambdas.shape[0],lambdas.shape[1]),np.nan)
    for i in np.arange(0,lambdas.shape[0]):
        for j in np.arange(0,lambdas.shape[1]):
            lam = np.nan_to_num(lambdas[i,j,:][bound : -bound])
            if lam.sum() != 0:
                p0, p1 = np.polyfit(tt[:-2], lam[:-2], 1)
                pv = kendall_tau_test(lam[:-2], 10000, p0)
                lambda_pvs[i,j]=pv
                lambda_trends[i,j]=p0
    return lambda_pvs, lambda_trends

def make_dataarray(ws=70,gedge=5):
    strn26_lambda = make_lambda_array(strn26,ws=ws)
    strn35_lambda = make_lambda_array(strn35,ws=ws)
    index_lambda = make_lambda_array(index,ws=ws)
    
    strn26_pvs,strn26_trends = make_pvs_array(strn26_lambda,yrs[gedge:-gedge],ws=ws)
    strn35_pvs,strn35_trends = make_pvs_array(strn35_lambda,yrs[gedge:-gedge],ws=ws)
    index_pvs,index_trends = make_pvs_array(index_lambda,yrs[gedge:-gedge],ws=ws)
    
    lams  = np.stack([strn26_lambda,strn35_lambda,index_lambda])
    pvs = np.stack([strn26_pvs,strn35_pvs,index_pvs])
    trends = np.stack([strn26_trends,strn35_trends,index_trends])

    lambdas = xr.Dataset(
            data_vars = dict(lambdas=(['indices','models','ensemble_members','time'],lams),
                            trends=(['indices','models','ensemble_members'],trends),
                            taus=(['indices','models','ensemble_members'],pvs)),
            coords = dict(
                    indices      = xr.DataArray(indices, dims="indices", coords=dict(indices=("indices", indices))),
                    time = xr.DataArray(yrs[gedge:-gedge], dims="time", coords=dict(time=("time", yrs[gedge:-gedge]))),
                    models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
                    ensemble_members = xr.DataArray(ensembs, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensembs)))),
        )
    return lambdas

if __name__ == "__main__":

    ds = xr.open_dataset('CMIP6_amoc.nc')

    strn26 = ds.strength_265N
    strn35 = ds.strength_35N
    index = ds.index
    yrs = strn26.year.values
    models = strn26.models.values
    ensembs = strn26.ensemble_members.values
    indices = ['strn26','strn35','index']

    for ws in [30,50,70]:
        lam = make_dataarray(ws=ws)
        lam.to_netcdf('new_EWS/CMIP6_lam_pvs_w{}.nc'.format(ws))
