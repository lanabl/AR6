import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
import scipy.stats as st
from scipy.ndimage import gaussian_filter1d
import time
import multiprocessing as mp
import itertools
from functools import partial
from mpi4py import MPI


def lambda_wrapper2(amoc,ws=50):
    amoc = np.nan_to_num(amoc)
    amoc_low = gaussian_filter1d(amoc, 10)
    if amoc.sum() != 0:
        lamb = run_fit_a_ar1((amoc-amoc_low),ws)
    else:
        lamb = np.full(len(amoc),np.nan)
    return lamb

def tau_wrapper(lambd,time,ws=50):
    bound = ws // 2
    tt = time[bound:-bound]
    lam = np.nan_to_num(lambd[bound : -bound])
    if lam.sum() != 0:
        tau = st.kendalltau(tt,lam)[0]
    else:
        tau = np.nan
    return tau

def slope_wrapper(lambd,time,ws=50):
    bound = ws // 2
    tt = time[bound:-bound]
    lam = np.nan_to_num(lambd[bound : -bound])
    if lam.sum() != 0:
        p0, p1, r, p, se = st.linregress(tt,lam)
    else:
        p0 = np.nan
    return p0



if __name__ == "__main__":
    for il in [1,2,3,4,5,6,7,8,9,10]:

        start_time = time.time()
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        print('Processor {}/{}'.format(rank,size))

        if rank==0:
            path = '/p/tmp/mayayami/AR6/CMIP6_surr_lambdas{}_full_w30.nc'.format(il)
            lams = xr.open_dataset(path).lams
            print(path)


            ##########
            data = lams[:,:,:,:,:]
            # data = lams
            ws = 30
            dim1, dim2, dim3, dim4, dim5 = data.shape[0],data.shape[1],data.shape[2],data.shape[3], data.shape[4]
            ##########
            if not dim1 % size == 0:
                exit()
            num_per_rank = dim1 /size
            print('{} tasks per processor'.format(num_per_rank))

            surr_list = [data.isel(surrogates=i).values for i in np.arange(0,dim1)]
            print(len(surr_list))
            in_list = [surr_list[i:i+int(num_per_rank)] for i in np.arange(0,dim1,int(num_per_rank))]
            print('List for scatter: size {}'.format(len(in_list)))
            if len(in_list)!=size:
                print('Mismatch between number of processors and list for scatter')
                exit()
        else:
            in_list = None
        v = comm.scatter(in_list,root=0)
        num_per_rank=len(v)
        dat = v[0]
        dim2, dim3, dim4, dim5 = dat.shape[0],dat.shape[1],dat.shape[2], dat.shape[3]
        print(dim2, dim3, dim4, dim5)
        print('Lists in scatter: size {}'.format(num_per_rank))

        t = np.arange(1850,2014)[5:-5]

        tres = []
        pres = []
        for l in np.arange(0,num_per_rank):
            print("Surrogate number {}/{} being done by processor {}/{}".format(l+1,num_per_rank,rank,size))
            lambds = v[int(l)]
            taus = np.full((dim2, dim3,dim4),np.nan)
            ps = np.full((dim2, dim3,dim4),np.nan)
            for i in np.arange(0,dim2):
                for j in np.arange(0,dim3):
                    for k in np.arange(0,dim4):
                        lambd = lambds[i,j,k]
                        taus[i,j,k] = tau_wrapper(lambd, t,ws=30)
                        ps[i,j,k] = slope_wrapper(lambd, t,ws=30)
            tres.append(taus)
            pres.append(ps)

        tres = np.array(tres)
        pres = np.array(pres)

        tg = comm.gather(tres,root=0)
        pg = comm.gather(pres,root=0)

        if comm.rank==0:
            ps = np.concatenate(pg)
            taus = np.concatenate(tg)

            ds = xr.open_dataset('CMIP6_amoc.nc')

            strn26 = ds.strength_265N
            strn35 = ds.strength_35N
            index = ds.index
            yrs = strn26.year.values
            models = strn26.models.values
            ensembs = strn26.ensemble_members.values
            indices = ['strn26','strn35','index']
            ns=dim1
            gedge=5

            lambda_array = xr.Dataset(
                data_vars = dict(ps=(['surrogates','indices','models','ensemble_members'],ps),
                            taus=(['surrogates','indices','models','ensemble_members'],taus)),
                coords = dict(
                        indices      = xr.DataArray(indices, dims="indices", coords=dict(indices=("indices", indices))),
                        surrogates = xr.DataArray(np.arange(0,ns), dims="surrogates", coords=dict(surrogates=("surrogates", np.arange(0,ns)))),
                        models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
                        ensemble_members = xr.DataArray(ensembs, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensembs)))),
            )
            print('made dataset')
            print("--- %s seconds ---" % (time.time() - start_time))
            opath = '/p/tmp/mayayami/AR6/CMIP6_surr_metrics{}_w30.nc'.format(il)
            lambda_array.to_netcdf(opath)
            print(opath)
            print('saved')
            print("--- %s seconds ---" % (time.time() - start_time))