#!/bin/bash


#SBATCH --job-name=sbpg
#SBATCH --output=%x-%j.out 
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=16
#SBATCH --account=tipes
#SBATCH --time=00-24:00:00
#SBATCH --error=%x-%j.err
#SBATCH --qos=priority


module purge
module load anaconda/5.0.0_py3 
source activate /p/tmp/mayayami/mayaenv
python get_subpolar_gyre_all_models.py
