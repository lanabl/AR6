import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
import scipy.stats as st
from scipy.ndimage import gaussian_filter1d
import time
import multiprocessing as mp
import itertools
from functools import partial
from mpi4py import MPI


def lambda_wrapper2(amoc,ws=50):
    amoc = np.nan_to_num(amoc)
    amoc_low = gaussian_filter1d(amoc, 10)
    if amoc.sum() != 0:
        lamb = run_fit_a_ar1((amoc-amoc_low),ws)
    else:
        lamb = np.full(len(amoc),np.nan)
    return lamb

def tau_wrapper(lambd,time,ws=50):
    bound = ws // 2
    tt = time[bound:-bound]
    lam = np.nan_to_num(lambd[bound : -bound])
    if lam.sum() != 0:
        tau = st.kendalltau(tt,lam)[0]
    else:
        tau = np.nan
    return tau

def slope_wrapper(lambd,time,ws=50):
    bound = ws // 2
    tt = time[bound:-bound]
    lam = np.nan_to_num(lambd[bound : -bound])
    if lam.sum() != 0:
        p0, p1, r, p, se = st.linregress(tt,lam)
    else:
        p0 = np.nan
    return p0



if __name__ == "__main__":
    il = 9
    

    start_time = time.time()

    path = '/p/tmp/mayayami/AR6/CMIP6_surr_lambdas{}_full.nc'.format(il)
    lams = xr.open_dataset(path).lams
    print(path)


    data = lams[:,:,:,:,:]
    # data = lams
    ws = 50
    dim1, dim2, dim3, dim4, dim5 = data.shape[0],data.shape[1],data.shape[2],data.shape[3], data.shape[4]


    print(dim1, dim2, dim3, dim4,dim5)


    t = np.arange(1850,2014)[5:-5]

    taus = np.full((dim1, dim2, dim3, dim4),np.nan)
    ps = np.full((dim1, dim2, dim3, dim4),np.nan)
    for l in np.arange(0,dim1):
        print("Surrogate number {}".format(l+1))
        for i in np.arange(0,dim2):
            for j in np.arange(0,dim3):
                for k in np.arange(0,dim4):
                    lambd = data[l,i,j,k]
                    taus[l,i,j,k] = tau_wrapper(lambd, t)
                    ps[l,i,j,k] = slope_wrapper(lambd, t)

    ds = xr.open_dataset('CMIP6_amoc.nc')

    strn26 = ds.strength_265N
    strn35 = ds.strength_35N
    index = ds.index
    yrs = strn26.year.values
    models = strn26.models.values
    ensembs = strn26.ensemble_members.values
    indices = ['strn26','strn35','index']
    ns=dim1
    gedge=5

    lambda_array = xr.Dataset(
        data_vars = dict(ps=(['surrogates','indices','models','ensemble_members'],ps),
                    taus=(['surrogates','indices','models','ensemble_members'],taus)),
        coords = dict(
                indices      = xr.DataArray(indices, dims="indices", coords=dict(indices=("indices", indices))),
                surrogates = xr.DataArray(np.arange(0,ns), dims="surrogates", coords=dict(surrogates=("surrogates", np.arange(0,ns)))),
                models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
                ensemble_members = xr.DataArray(ensembs, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensembs)))),
    )
    print('made dataset')
    print("--- %s seconds ---" % (time.time() - start_time))
    opath = '/p/tmp/mayayami/AR6/CMIP6_surr_metrics{}.nc'.format(il)
    lambda_array.to_netcdf(opath)
    print(opath)
    print('saved')
    print("--- %s seconds ---" % (time.time() - start_time))