
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
from jupyterthemes import jtplot
import matplotlib as mpl
import pandas as pd
import datetime
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cartopy.crs as ccrs
import os
import glob
import json
import xarray as xr


# GLOBAL VARIABLES
# Region
lat_max = 61
lat_min = 46
lon_max = 360-20
lon_min = 360-55
lon_max_W = -20
lon_min_W = -55

# these seem to be the three latitude/longitude keys
lat_keys = ['lat', 'latitude', 'nav_lat']
lon_keys = ['lon', 'longitude', 'nav_lon']


## FUNCTIONS TO SPECIFY/EDIT THE GLOBAL VARIABLES
# set parameters
def set_boundaries(
    longitude_maximum = 360-20,
    longitude_minimum = 360-55,
    latitude_maximum = 61,
    latitude_minimum = 46):

    global lon_max, lon_min, lat_max, lat_min, lon_max_W, lon_min_W
    lon_max = longitude_maximum
    lon_min = longitude_minimum 
    lat_max = latitude_maximum
    lat_min = latitude_minimum
    lon_max_W = lon_max-360
    lon_min_W = lon_min-360

def set_lon_lat_keys(
    longitudes=['lon', 'longitude', 'nav_lon'],
    latitudes =['lat', 'latitude', 'nav_lat']):

    global lon_keys, lat_keys
    lon_keys = longitudes
    lat_keys = latitudes

def get_values():
    print("lon_min: "+str(lon_min))
    print("lon_max: "+str(lon_max))
    print("lat_min: "+str(lat_min))
    print("lat_max: "+str(lat_max))
    print("lon_keys: "+str(lon_keys))
    print("lat_keys: "+str(lat_keys))


## FILL THE AMOC_INDEX-FILE WITH THE SUBPOLAR GYRE OF THE CMIP6 MODELS
# function to get the longitude / latitude keys from the model
def get_lon_lat_keys(data, location_keys):
    str_loc = [key for key in data.coords.keys() if key in location_keys]
    if len(str_loc) != 1:
        print('CAUTION: There exist the keys: ' + str(str_loc) + '. The first one is used!')
    return str_loc[0]

# let all datasets have the same representation of time
def time_to_year_month(data):
    return data.assign_coords(dict(time = [np.datetime64(d, 'M') for d in data.time.values]))

# function to extract subpolar gyre mean
def subpolargyre(data, str_lon, str_lat, print_info=False):
    mean_dims = [d for d in data.dims if d != "time"]
    if print_info:
        print("   The mean is taken over the dimensions ", mean_dims)
    if np.sum(data[str_lon].values<0) > 0:
        lon_max_tmp = lon_max_W
        lon_min_tmp = lon_min_W
    else:
        lon_max_tmp = lon_max
        lon_min_tmp = lon_min
    spg = (
        data
        .where(data[str_lon] >= lon_min_tmp)
        .where(data[str_lon] <= lon_max_tmp)
        .where(data[str_lat] >= lat_min)
        .where(data[str_lat] <= lat_max)
    )
    spg = spg.mean(dim = mean_dims)
    gl = data.mean(dim = mean_dims)
    spg = spg - gl
    # spg = spg.groupby('time.year').mean() # that would produce yearly means
    return spg

# wrapper to get the spg mean for an ensemble member
def get_subpolar_gyre(model, emember, print_info=False, paths_all_models=None):
    if paths_all_models is None:
        paths_all_models = get_paths_dict()
    paths = paths_all_models[model][emember]

    if len(paths) != 1:
        # there is one model where there are 2 paths for the same ensemble member...
        print("CAUTION: The model '" + model + "' provides " + str(len(paths)) + " paths for the ensemble member '" + emember + "'. It is NOT added to CMIP6_amoc_index.nc, this must be done manually!")
        return None
    if model == 'EC-Earth3-Veg' and emember == 'r10i1p1f1':
        print("   Ensemble-member '"+emember+ "' does not provide all years. It also does not occur in Matthew's data, so its not included here.")
        return None
    else:
        data = xr.open_mfdataset(paths[0], concat_dim="time").tos
        if model == 'CNRM-CM6-1-HR':
            # this crashes with a oom-kill, so re-chunking with 2-timestep-chunks seems to help...
            data = data.chunk(dict(time=2))

        str_lon = get_lon_lat_keys(data, lon_keys)
        str_lat = get_lon_lat_keys(data, lat_keys)
        if print_info:
            print('   + Longitude: ' + str_lon)
            print('   + Latitude:  ' + str_lat)

        spg = subpolargyre(data, str_lon, str_lat, print_info)
        spg = time_to_year_month(spg)

        if print_info: print('  Ensemble-members added:')
        print('    - ' +emember)

        return spg


# set everything to nan where there are no full timeseries for the AMOC strength at 26.5N and 35N
def remove_incomplete_timeseries(amoc_index):
    amoc = get_amoc_strengths(amoc_index)
    amoc_strength_265 = amoc.amoc_strength_265.astype(np.float)
    amoc_strength_35  = amoc.amoc_strength_35.astype(np.float)
    
    incomplete = (np.isnan(amoc_strength_265).sum(dim="year") > 0)  & (np.isnan(amoc_strength_35).sum(dim="year") > 0 )

    for m in range(len(amoc_strength_265.models.values)):
        for em in range(len(amoc_strength_265.ensemble_members.values)):
            if incomplete.isel(models=m, ensemble_members=em):
                amoc_strength_265.isel(models=m, ensemble_members=em).values.fill(np.nan)
                amoc_strength_35.isel(models=m, ensemble_members=em).values.fill(np.nan)
                amoc_index.isel(models=m, ensemble_members=em).values.fill(np.nan)

    # remove the UKESM1-0-LL r1i1p1f1 in the amoc-strength data
    model   = 'UKESM1-0-LL'
    emember = 'r1i1p1f1'
    amoc_strength_265.sel(models = model, ensemble_members = emember).values.fill(np.nan)
    amoc_strength_35.sel(models = model, ensemble_members = emember).values.fill(np.nan)

    # save all CMIP6 amoc data in one file
    xr.Dataset(dict(
        strength_265N = amoc_strength_265,
        strength_35N  = amoc_strength_35,
        index         = amoc_index.groupby('time.year').mean(dim="time")
    )).to_netcdf("CMIP6_amoc.nc", mode='w')
    return amoc_index


# getMatthew's AMOC strength
def get_amoc_strengths(amoc_index):
    with open('matthew/JSON_data/Figure_AR6_CMIP5-6_AMOC_35N_1000m.json', 'r') as handle:
        json_load = json.load(handle)

    amoc_c6_ts = np.ma.asarray(json_load["amoc_c6_ts"])
    cmip6_models = json_load["cmip6_models"]
    year = np.asarray(json_load["year"])
    experiments = ['ssp119', 'ssp126', 'ssp245', 'ssp370', 'ssp585']
    latitudes   = ['26.5N', '35N']
    ''' 
    Ensemble members:
    These are the first 10 ensemble members r${ens_num}i1p1f1 in the respective experiments. 
    Where "f1" was not available we have used "f2" or "f3" and so on.
    '''
    amoc_strength = xr.DataArray(
        amoc_c6_ts,
        dims = ('models', 'experiments', 'ensemble_members', 'latitudes', 'year'),
        coords = dict(
            models      = xr.DataArray(cmip6_models, dims="models", coords=dict(models=("models", cmip6_models))),
            experiments = xr.DataArray(experiments, dims="experiments", coords=dict(experiments=("experiments", experiments))),
            latitudes   = xr.DataArray(latitudes, dims="latitudes", coords=dict(latitudes=("latitudes", latitudes))),
            year        = xr.DataArray(year, dims="year", coords=dict(year=("year", year))),
            ensemble_members = amoc_index.ensemble_members
            )
        )
    amoc_index = amoc_index.groupby('time.year').mean(dim="time")

    # the historical period is same over all experiments, so just choose any
    return xr.Dataset(
        dict(
            amoc_strength_265 = amoc_strength.sel(latitudes='26.5N', experiments='ssp119', drop=True).sel(year=amoc_index.year),
            amoc_strength_35  = amoc_strength.sel(latitudes='35N',   experiments='ssp119', drop=True).sel(year=amoc_index.year)
        )
    )


## CREATE NEW OUTPUT FILE
def create_new_amoc_index_file(output_file):
    nparray = np.empty((34, 10, 165*12))
    nparray.fill(np.nan)
    dims = ('models', 'ensemble_members', 'time')
    models = ['AWI-CM-1-1-MR', 'BCC-CSM2-MR', 'BCC-ESM1', 'CAMS-CSM1-0',
            'FGOALS-f3-L', 'FGOALS-g3', 'IITM-ESM', 'CanESM5', 'CNRM-CM6-1',
            'CNRM-CM6-1-HR', 'CNRM-ESM2-1', 'E3SM-1-1', 'EC-Earth3',
            'EC-Earth3-Veg', 'FIO-ESM-2-0', 'INM-CM4-8', 'INM-CM5-0',
            'IPSL-CM6A-LR', 'MIROC6', 'HadGEM3-GC31-LL', 'HadGEM3-GC31-MM',
            'UKESM1-0-LL', 'MPI-ESM1-2-HR', 'MRI-ESM2-0', 'GISS-E2-1-G', 'CESM2',
            'CESM2-WACCM', 'NorESM1-F', 'NorESM2-LM', 'GFDL-AM4', 'GFDL-CM4',
            'GFDL-ESM4', 'NESM3', 'SAM0-UNICON']
    ensemble_members = ['r1i1p1f1', 'r2i1p1f1', 'r3i1p1f1', 'r4i1p1f1', 'r5i1p1f1', 'r6i1p1f1',
            'r7i1p1f1', 'r8i1p1f1', 'r9i1p1f1', 'r10i1p1f1']
    time = np.arange(np.datetime64('1850-01'), np.datetime64('2015-01'))


    amoc_index = xr.DataArray(
        nparray,
        dims = dims,
        coords = dict(
            models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
            time        = xr.DataArray(time, dims="time", coords=dict(time=("time", time))),
            ensemble_members = xr.DataArray(ensemble_members, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensemble_members))),
            )
        )
    amoc_index.to_netcdf(output_file)


## PLOTS
# make plots of world
def make_nice_plot(ds, str_lon, str_lat):
    fig, axis = plt.subplots(
        1, 1, subplot_kw=dict(projection=ccrs.Orthographic(central_longitude=320))
    )
    ds.plot(
        x = str_lon, 
        y = str_lat,
        ax = axis,
        transform = ccrs.PlateCarree()
    )
    axis.coastlines(linewidth=2,alpha=0.6)
    return fig

# make nice plots of spg area
def make_nice_plot_of_spg(data, str_lon, str_lat, model=None):
    if np.sum(data[str_lon].values<0) > 0:
        lon_max_tmp = lon_max_W
        lon_min_tmp = lon_min_W
    else:
        lon_max_tmp = lon_max
        lon_min_tmp = lon_min
    spg = (
        data
        .where(data[str_lon] >= lon_min_tmp)
        .where(data[str_lon] <= lon_max_tmp)
        .where(data[str_lat] >= lat_min)
        .where(data[str_lat] <= lat_max)
    )
    fig, axis = plt.subplots(
        1, 1, subplot_kw=dict(projection=ccrs.Orthographic(central_longitude=320))
    )
    spg.plot(
        x = str_lon, 
        y = str_lat,
        ax = axis,
        transform = ccrs.PlateCarree()
    )
    axis.coastlines(linewidth=2,alpha=0.6)
    if not model is None:
        fig.suptitle(model)
    return fig

def get_paths_dict():
    path_base = '/p/tmp/mayayami/SYNDA/data/CMIP6/CMIP/'
    institutes = os.listdir(path_base)
    paths_all_models = dict()

    for i in institutes:
        pi = path_base + i
        models = os.listdir(pi)
        for m in models:
            pim = pi + '/' + m + '/historical/'
            emembers = os.listdir(pim)
            tmp = dict()
            for v in emembers:
                paths = [x[0] for x in os.walk(pim+v)]
                data_paths = []
                for path in paths:
                    if glob.glob("{}/tos_Omon_*.nc".format(path)):
                        data_paths.append(glob.glob("{}/tos_Omon_*.nc".format(path)))
                if (len(data_paths)) != 1:
                    print("Several paths provided for model '"+m+"' and ensemble member '"+v+"'!")
                    print(data_paths)
                tmp[v] = data_paths
            paths_all_models[m] = tmp
    return paths_all_models
