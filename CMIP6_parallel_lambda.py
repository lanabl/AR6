import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
import scipy.stats as st
from scipy.ndimage import gaussian_filter1d
import time
import multiprocessing as mp
import itertools
from functools import partial

def make_lambdas(data,ws=50,gedge=5):
    lambdas = np.full((data.shape[0],data.shape[1],data.shape[2],data.shape[3],data.shape[4]),np.nan)
    for i in np.arange(0,data.shape[0]):
        print('indice {} done'.format(i))
        for j in np.arange(0,data.shape[1]):
            for k in np.arange(0,data.shape[2]):
                for l in np.arange(0,data.shape[3]):
                    amoc = np.nan_to_num(data[i,j,k,l].values)
                    amoc_low = gaussian_filter1d(amoc, 10)
                    if amoc.sum() != 0:
                        lamb = run_fit_a_ar1((amoc-amoc_low),ws)
                        lambdas[i,j,k,l,:]=lamb
    return lambdas

def make_lambdas2(data,ws=50,gedge=5):
    lambdas = np.full((data.shape[0].data.shape[1]),np.nan)
    for i in np.arange(0,data.shape[0]):
        amoc = np.nan_to_num(data[i].values)
        amoc_low = gaussian_filter1d(amoc, 10)
        if amoc.sum() != 0:
            lamb = run_fit_a_ar1((amoc-amoc_low),ws)
            lambdas[i,:]=lamb
    return lambdas

def lambda_wrapper(indices,data,ws=50):
    i,j,k,l = indices[0], indices[1], indices[2], indices[3]
    amoc = np.nan_to_num(data[i,j,k,l].values)
    amoc_low = gaussian_filter1d(amoc, 10)
    if amoc.sum() != 0:
        lamb = run_fit_a_ar1((amoc-amoc_low),ws)
    else:
        lamb = np.full(data.shape[4],np.nan)
    return lamb

if __name__ == "__main__":
    start_time = time.time()
    p = mp.Pool()
    
    surrs = xr.open_dataset('/p/tmp/mayayami/CMIP6_surrogates2.nc').surrs
    model = 'CESM2'
    iens = 0
    indx = 'strn26'
    surr = surrs.sel(models=model).isel(ensemble_members=iens).sel(indices=indx)

    ##########
    # data = surrs[:,:,:,:]
    data = surrs[:,:,:,:2,:]
    # data = xr.open_dataset('/p/tmp/mayayami/CMIP6_surrogates2.nc').surrs
    ws = 50
    ##########

    dim1, dim2, dim3, dim4, dim5 = data.shape[0],data.shape[1],data.shape[2],data.shape[3], data.shape[4]
    input = ((i,j,k,l) for i,j,k,l in itertools.combinations_with_replacement(np.arange(dim4), 4) if i < dim1 and j < dim2 and k < dim3)
    print(dim1, dim2, dim3, dim4)
    # lambdas = np.full((dim1, dim2, dim3, dim4,dim5),np.nan)

    results = np.full((dim1*dim2*dim3*dim4,dim5),np.nan)
    res = p.imap(partial(lambda_wrapper,data=data), input)
    for i,r in enumerate(res):
        results[i,:] = r
    results.shape = (dim1,dim2,dim3, dim4,dim5)
    lambdas = results


    print('made lambdas')
    ds = xr.open_dataset('CMIP6_amoc.nc')

    strn26 = ds.strength_265N
    strn35 = ds.strength_35N
    index = ds.index
    yrs = strn26.year.values
    models = strn26.models.values
    ensembs = strn26.ensemble_members.values
    indices = ['strn26','strn35','index']
    ns=100
    gedge=5

    lambda_array = xr.Dataset(
        data_vars = dict(lams=(['indices','models','ensemble_members','surrogates','time'],lambdas)),
        coords = dict(
                indices      = xr.DataArray(indices, dims="indices", coords=dict(indices=("indices", indices))),
                time = xr.DataArray(yrs[gedge:-gedge][:-1], dims="time", coords=dict(time=("time", yrs[gedge:-gedge][:-1]))),
                surrogates = xr.DataArray(np.arange(0,ns), dims="surrogates", coords=dict(surrogates=("surrogates", np.arange(0,ns)))),
                models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
                ensemble_members = xr.DataArray(ensembs, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensembs)))),
    )
    print('made dataset')
    print("--- %s seconds ---" % (time.time() - start_time))

    lambda_array.to_netcdf('/p/tmp/mayayami/CMIP6_surr_lambdas_small3.nc')
    print('saved')
    print("--- %s seconds ---" % (time.time() - start_time))