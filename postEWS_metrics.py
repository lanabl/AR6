import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
import scipy.stats as st
from scipy.ndimage import gaussian_filter1d
import time
import multiprocessing as mp
import itertools
from functools import partial
from mpi4py import MPI


def lambda_wrapper2(amoc,ws=50):
    amoc = np.nan_to_num(amoc)
    amoc_low = gaussian_filter1d(amoc, 10)
    if amoc.sum() != 0:
        lamb = run_fit_a_ar1((amoc-amoc_low),ws)
    else:
        lamb = np.full(len(amoc),np.nan)
    return lamb

def tau_wrapper(lambd,time):
    tt = time
    lam = np.nan_to_num(lambd)
    if lam.sum() != 0:
        tau = st.kendalltau(tt,lam)[0]
    else:
        tau = np.nan
    return tau

def slope_wrapper(lambd,time):
    tt = time
    lam = np.nan_to_num(lambd)
    if lam.sum() != 0:
        p0, p1, r, p, se = st.linregress(tt,lam)
    else:
        p0 = np.nan
    return p0

if __name__ == "__main__": 
    for il in np.arange(1,11):
        #################
        ns = 1000
        ws =30
        bound = ws // 2
        #############

        start_time = time.time()
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        print('Processor {}/{}'.format(rank,size))

        if rank==0:
            path = '/home/mayayami/AR6/new_EWS/CMIP6_lam_pvs_w30.nc'
            lams = xr.open_dataset(path).lambdas
            print(path)


            ##########
            # data = surrs[:,:,:,:]
            # data = surrs[:,:,:,:16,:]
            data = lams

            dim1, dim2, dim3, dim4 = data.shape[0],data.shape[1],data.shape[2],data.shape[3]
            ##########
            if not dim2 % size == 0:
                print('ouch!')
                exit()
            num_per_rank = dim2 /size
            print('{} tasks per processor'.format(num_per_rank))

            mod_list = [data.isel(models=i).values for i in np.arange(0,dim2)]
            print(len(mod_list))
            in_list = [mod_list[i:i+int(num_per_rank)] for i in np.arange(0,dim2,int(num_per_rank))]
            print('List for scatter: size {}'.format(len(in_list)))
            if len(in_list)!=size:
                print('Mismatch between number of processors and list for scatter')
                exit()
        else:
            in_list = None
        v = comm.scatter(in_list,root=0)

        num_per_rank=len(v)
        dat = v[0]
        dim1, dim3, dim4 = dat.shape[0],dat.shape[1],dat.shape[2]
        print(dim1, dim3, dim4)
        print('Lists in scatter: size {}'.format(num_per_rank))
        
        t = np.arange(1850,2014)[5:-5][bound : -bound]

        sres = []
        tres = []
        pres = []
        ares= []
        for l in np.arange(0,num_per_rank):
            print("Model number {}/{} being done by processor {}/{}".format(l+1,num_per_rank,rank,size))
            lambds = v[int(l)]
            surrs = np.full((ns, dim1, dim3, dim4-1-ws),np.nan)
            # taus = np.full((ns, dim1, dim3),np.nan)
            ass = np.full((ns, dim1, dim3),np.nan)
            # ps = np.full((ns, dim1, dim3),np.nan)
            for i in np.arange(0,dim1):
                for j in np.arange(0,dim3):
                    lambd = np.nan_to_num(lambds[i,j][bound : -bound])
                    surrs[:,i,j,:]=fourrier_surrogates(lambd,ns)
                    for k in np.arange(0,ns):
                        lam = np.nan_to_num(surrs[k,i,j])
                        ass[k,i,j]=sm.tsa.acf(lam,nlags=1)[1]
                        # taus[k,i,j] = tau_wrapper(lam, t)
                        # ps[k,i,j] = slope_wrapper(lam, t)
            sres.append(surrs)
            # tres.append(taus)
            # pres.append(ps)
            ares.append(ass)
        print('made lambdas')
        # tres = np.array(tres)
        # pres = np.array(pres)
        sres = np.array(sres)
        print(sres.shape)

        # tg = comm.gather(tres,root=0)
        # pg = comm.gather(pres,root=0)
        sg = comm.gather(sres,root=0)
        ag = comm.gather(ares,root=0)

        if comm.rank==0:
            # ps = np.concatenate(pg)
            # taus = np.concatenate(tg)
            surrs = np.concatenate(sg)
            ass = np.concatenate(ag)

            ds = xr.open_dataset('CMIP6_amoc.nc')

            strn26 = ds.strength_265N
            strn35 = ds.strength_35N
            index = ds.index
            yrs = strn26.year.values
            models = strn26.models.values
            ensembs = strn26.ensemble_members.values
            indices = ['strn26','strn35','index']
            gedge=5

            lambda_array = xr.Dataset(
                data_vars = dict(surrs=(['models','surrogates','indices','ensemble_members','time'],surrs),
                                a=(['models','surrogates','indices','ensemble_members'],ass)),
                coords = dict(
                        indices      = xr.DataArray(indices, dims="indices", coords=dict(indices=("indices", indices))),
                        time = xr.DataArray(t, dims="time", coords=dict(time=("time", t))),
                        surrogates = xr.DataArray(np.arange(0,ns), dims="surrogates", coords=dict(surrogates=("surrogates", np.arange(0,ns)))),
                        models      = xr.DataArray(models, dims="models", coords=dict(models=("models", models))),
                        ensemble_members = xr.DataArray(ensembs, dims="ensemble_members", coords=dict(ensemble_members=("ensemble_members", ensembs)))),
            )
            print('made dataset')
            print("--- %s seconds ---" % (time.time() - start_time))
            opath = '/p/tmp/mayayami/AR6/CMIP6_surr_autc_postEWS{}_w30.nc'.format(il)
            lambda_array.to_netcdf(opath)
            print(opath)
            print('saved')
            print("--- %s seconds ---" % (time.time() - start_time))