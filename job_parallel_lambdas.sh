#!/bin/bash


#SBATCH --job-name=xrlambda
#SBATCH --output=%x-%j.out 
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --account=tipes
#SBATCH --time=00-01:00:00
#SBATCH --error=%x-%j.err
#SBATCH --qos=priority

module load anaconda
source activate envAMOC
python CMIP6_parallel_lambda_xarray.py
