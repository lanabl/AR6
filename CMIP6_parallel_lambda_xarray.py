
import matplotlib.pyplot as plt
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
import scipy.stats as st
from scipy.ndimage import gaussian_filter1d
import time
import multiprocessing as mp
import itertools
from functools import partial




def gaussian_filter1d_wrapper(d):
    # calculates the residual after STL on one single np.array
    d.flags.writeable = True # the key to resolve "ValueError: buffer source array is read-only"
    d = np.nan_to_num(d)
    return d - gaussian_filter1d(d, 10)

def detrend(d, time_dim='time'):
    # applies STLresid to a dask-chunked xr.DataArray
    return xr.apply_ufunc(
        gaussian_filter1d_wrapper, 
        d,
        input_core_dims=[[time_dim]],   # dimension not to "broadcast" (=loop) over
        output_core_dims=[[time_dim]],  # dimension that the output will have
        keep_attrs=True,
        dask='parallelized',            # as STLresid is not dask-conform, parallelize it
        vectorize=True,                 # vectorize to apply it on single lat-lon-combinations
        output_dtypes=[float]           # with dask="parallelized" we have to provide that
        )
    


def lambda_wrapper(d):
    if d.sum() == 0: return d
    l =  run_fit_a_ar1(d, ws)
    return l

def calculate_lambdas(d, time_dim='time'):
    return xr.apply_ufunc(
        lambda_wrapper, 
        d,
        input_core_dims=[[time_dim]],   # dimension not to "broadcast" (=loop) over
        output_core_dims=[[time_dim]],  # dimension that the output will have
        keep_attrs=True,
        dask='parallelized',            # as run_fit_a_ar1 is not dask-conform, parallelize it
        vectorize=True,                 # vectorize to apply it on single models, ...
        output_dtypes=[float]           # with dask="parallelized" we have to provide that
        )


start_time = time.time()
# p = mp.Pool()

# surrs = xr.open_dataset('/p/tmp/mayayami/CMIP6_surrogates2.nc').surrs
# model = 'CESM2'
# iens = 0
# indx = 'strn26'
# surr = surrs.sel(models=model).isel(ensemble_members=iens).sel(indices=indx)

##########
# data = surrs[:,:,:,:]
# data = surrs[:,:,:,:100,:] #  indices x models x ensemble_members x surrogates x time
data = xr.open_dataset('/p/tmp/mayayami/CMIP6_surrogates2.nc').surrs
ws = 50
##########

dsmall = data.isel(surrogates=range(10))#.isel(surrogates=[0,1], models=range(5))
dsmall

print("- ZEROS instead of NaNs")

# exclude_full_zeros = (dsmall!=0).sum("time") > 0
# dsmall = dsmall.where(exclude_full_zeros)


start_time2 = time.time()

res = detrend(dsmall)

print("- CHUNKED: models:1")
res = res.chunk({"models": 1})

print(res)


# TODO: probably we might want to chunk it
# res = res.chunk({"models":1})  
lambdas = calculate_lambdas(res)


lambdas.compute()

print('made dataset')
print("--- %s seconds ---" % (time.time() - start_time))
print("--- %s seconds ---" % (time.time() - start_time2))

# lambdas.to_netcdf('/p/tmp/mayayami/CMIP6_surr_lambdas_xr.nc')
# print('saved')
# print("--- %s seconds ---" % (time.time() - start_time))


