#!/bin/bash 
#SBATCH --qos=priority 
#SBATCH --job-name=job3
#SBATCH --account=tipes
#SBATCH --mem=10000
#SBATCH --output=job3-%j.log
#SBATCH --error=job3-%j.err
#SBATCH --workdir=/home/mayayami/AR6/


srun python CMIP6_new_lambda.py