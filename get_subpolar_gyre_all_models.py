# # Get subpolar gyre mean for each ensemble member

import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
from jupyterthemes import jtplot
import matplotlib as mpl
import pandas as pd
import datetime
from mpl_toolkits.axes_grid1 import make_axes_locatable
import cartopy.crs as ccrs
import cartopy
import os
import glob
import xarray as xr
from get_subpolar_gyre_functions import *


# file where to save results
output_file = "CMIP6_amoc_index.nc"
# should the output file be overwritten or should the values be filled in the exisitng one
overwrite_output_file = True

# Regional variables
latitude_maximum = 61
latitude_minimum = 46
longitude_maximum = 360-20
longitude_minimum = 360-55

set_boundaries(
    longitude_maximum = longitude_maximum,
    longitude_minimum = longitude_minimum,
    latitude_maximum = latitude_maximum,
    latitude_minimum = latitude_minimum
)

# these seem to be the three latitude/longitude keys
lat_keys = ['lat', 'latitude', 'nav_lat']
lon_keys = ['lon', 'longitude', 'nav_lon']

set_lon_lat_keys(
    longitudes=lon_keys,
    latitudes =lat_keys
)

get_values()


# Get paths as dict of models and ensemble_members
paths_all_models = get_paths_dict()

# Load shared amoc index file
if overwrite_output_file:
    create_new_amoc_index_file(output_file)
amoc_index = xr.open_dataarray(output_file)
amoc_index = time_to_year_month(amoc_index)


# # Loop over all models and ensemble members
for model, emembers in paths_all_models.items():
    print('------------------------------------------')
    print(model)
    print_info = True
    for emember, paths in emembers.items():
        # the models were only f2 or f3 are available still have the "f1" coordinates in the amoc_index file
        emember_in_amoc_index = emember[:-1]+"1" 

        spg = get_subpolar_gyre(
            model   = model,
            emember =  emember, 
            print_info = print_info, 
            paths_all_models = paths_all_models)
    
        if not spg is None:
            print_info = False
            amoc_index.loc[dict(models=model, ensemble_members=emember_in_amoc_index)] = spg
    
    # remove all members where we have no or incomplete timeseries in AMOC strength
    # this function also saves (the annual) amoc_index, amoc_strength @26.5N and amoc_strength @35N into a single dataset called "CMIP6_amoc.nc"
    amoc_index = remove_incomplete_timeseries(amoc_index)


# Save the amoc index file with its new values            
amoc_index.to_netcdf(output_file)


