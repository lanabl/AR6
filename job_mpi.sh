#!/bin/bash 
#SBATCH --qos=priority 
#SBATCH --job-name=job_mpi
#SBATCH --account=tipes
#SBATCH --mem=10000
#SBATCH --nodes=17
#SBATCH --ntasks-per-node=2
#SBATCH --mpi=pmi2
#SBATCH --output=ajob-%j.log
#SBATCH --error=ajob-%j.err
#SBATCH --workdir=/home/mayayami/AR6/

echo "------------------------------------------------------------"
echo "SLURM JOB ID: $SLURM_JOBID"
echo "$SLURM_NTASKS tasks"
echo "------------------------------------------------------------"

module load intel/2017.1

export I_MPI_PMI_LIBRARY=/p/system/slurm/lib/libpmi.so

# mpirun -bootstrap slurm -n $SLURM_NTASKS python CMIP6_parallel_lambda2.py

# mpirun -bootstrap slurm -n $SLURM_NTASKS python CMIP6_parallel_metric.py

mpirun -bootstrap slurm -n $SLURM_NTASKS python postEWS_metrics.py

# mpirun -bootstrap slurm -n $SLURM_NTASKS python CMIP6_parallel_ar1.py


