from sre_constants import NOT_LITERAL
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_process import ArmaProcess
import xarray as xr
from EWS_functions import *
from scipy.stats import linregress
from scipy.ndimage import gaussian_filter1d


def make_lambda_array(data,ws=70,gedge=5,gsigma=10):
    lambdas = np.full((data.shape[0],data.shape[1]-2*gedge),np.nan)
    for i in np.arange(0,data.shape[0]):
        amoc = np.nan_to_num(data[i])
        amoc_low = gaussian_filter1d(amoc, gsigma)
        if amoc.sum() != 0:
            lamb = run_fit_a_ar1((amoc-amoc_low)[gedge:-gedge],ws)
            lambdas[i,:]=lamb
    return lambdas

def make_kendall_array(lambdas,time,ws=70):
    bound = ws // 2
    tt = time[bound:-bound]
    lambda_pvs = np.full((lambdas.shape[0]),np.nan)
    for i in np.arange(0,lambdas.shape[0]):
        lam = np.nan_to_num(lambdas[i,:][bound : -bound])
        if lam.sum() != 0:
            tau = st.kendalltau(tt,lam)
            lambda_pvs[i]=tau[0]
    return lambda_pvs

def make_dataarray(data,time,ws=70,gedge=5):
    lamss = np.zeros((3,niter,ntime-2*gedge))
    for i in range(3):
        data = fakess[i]
        lamss[i] = make_lambda_array(data, ws,gedge)
        
    tau = np.zeros((3,niter))
    for i in range(3):
        data = lamss[i]
        tau[i] = make_kendall_array(data,time[gedge:-gedge],ws=ws)
        
    lambdas = xr.Dataset(
        data_vars = dict(lambdas=(['a','iters','time'],lamss),
                        taus = (['a','iters'],tau)),
        coords = dict(
                time = xr.DataArray(time[5:-5], dims="time", coords=dict(time=("time", time[5:-5]))),
                a      = xr.DataArray([0.5,0.7,0.9], dims="a", coords=dict(a=("a", [0.5,0.7,0.9]))),
                iters = xr.DataArray(np.arange(0,niter), dims="iters", coords=dict(iters=("iters", np.arange(0,niter)))))
    )
    return lambdas


if __name__ == "__main__":

    ntime = 165
    niter = 5000
    yrs = np.arange(1850,2015)
    fakess = np.zeros((3,niter,ntime))
    for i ,a in enumerate([0.5,0.7,0.9]):
        ar1 = np.array([1, -a])
        ma1 = np.array([1])
        AR_object1 = ArmaProcess(ar1, ma1)

        fakess[i,:] = AR_object1.generate_sample(nsample=[niter,ntime],axis=1)

    # for ws in [30,50,70]:
    for ws in [50,70,30]:
        lam = make_dataarray(fakess,yrs,ws=ws)
        lam.to_netcdf('new_EWS/lam_tau_w{}_extra2.nc'.format(ws))

    